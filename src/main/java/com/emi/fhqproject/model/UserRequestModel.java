package com.emi.fhqproject.model;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

import com.emi.fhqproject.util.FieldsValueMatch;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@FieldsValueMatch.List({
    @FieldsValueMatch(
        field = "password",
        fieldMatch = "verifyPassword",
        message = "Password fields must match!"
    )
})
public class UserRequestModel {

    @NotBlank
    private String name;

    @NotBlank
    private String username;

    @NotBlank
    @Email
    private String email;

    @NotBlank
	@Pattern(regexp = "(^[0-9]+$|^$)", message = "number only")
    private String phone;

    @NotBlank
    private String password;

    @NotBlank
    private String verifyPassword;

}