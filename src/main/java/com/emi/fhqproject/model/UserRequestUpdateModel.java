package com.emi.fhqproject.model;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class UserRequestUpdateModel extends UserRequestCreateModel{

    @NotNull
    private String id;
    
}