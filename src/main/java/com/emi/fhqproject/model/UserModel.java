package com.emi.fhqproject.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class UserModel extends PersistenceModel{
    private String name;
    private String username;
    private String password;
    private String email;
    private String phone;
    
}