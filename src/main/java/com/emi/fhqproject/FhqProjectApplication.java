package com.emi.fhqproject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class FhqProjectApplication {
	public static void main(String[] args) {
		SpringApplication.run(FhqProjectApplication.class, args);
	}
}
