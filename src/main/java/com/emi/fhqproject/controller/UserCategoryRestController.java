package com.emi.fhqproject.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import com.emi.fhqproject.model.UserModel;
import com.emi.fhqproject.model.UserRequestCreateModel;
import com.emi.fhqproject.model.UserRequestUpdateModel;
import com.emi.fhqproject.service.UserCategoryService;

import org.springframework.http.HttpStatus;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;

@Api
@RestController
@RequestMapping("api/rest/user-category")
public class UserCategoryRestController {

    @Autowired
    private UserCategoryService userCategoryService;

    @PreAuthorize("hasRole('ROLE_ADMIN')")
	@PostMapping("/save")
	public UserModel save(@RequestBody @Valid UserRequestCreateModel request, 
			BindingResult result,
			HttpServletResponse response) throws IOException {
		UserModel userModel = new UserModel();
		if (result.hasErrors()) {
			response.sendError(HttpStatus.BAD_REQUEST.value(), result.getAllErrors().toString());
			return userModel;
		} else {
			BeanUtils.copyProperties(request, userModel);
			return userCategoryService.saveOrUpdate(userModel); 
		}
    }
    
    @PreAuthorize("hasRole('ROLE_ADMIN')")
	@PostMapping("/update")
	public UserModel update(@RequestBody @Valid UserRequestUpdateModel request, 
			BindingResult result,
			HttpServletResponse response) throws IOException {
		UserModel userModel = new UserModel();
		if (result.hasErrors()) {
			response.sendError(HttpStatus.BAD_REQUEST.value(), result.getAllErrors().toString());
			return userModel;
		} else {
			BeanUtils.copyProperties(request, userModel);
			return userCategoryService.saveOrUpdate(userModel);
		}
    }
    
    @PreAuthorize("hasRole('ROLE_ADMIN')")
	@DeleteMapping("/deleteById/{id}")
	public UserModel delete(@PathVariable("id") final String id) {
		return userCategoryService.deleteById(id);
	}

	@PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_CLIENT', 'ROLE_USER')")
	@GetMapping("/findAll")
	public List<UserModel> findAll() {
		return userCategoryService.findAll();
	}
	
	@PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_CLIENT', 'ROLE_USER')")
	@GetMapping("/findById/{id}")
	public UserModel findById(@PathVariable("id") final String id) {
		return userCategoryService.findById(id);
	}
}
