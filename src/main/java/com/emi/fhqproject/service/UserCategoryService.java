package com.emi.fhqproject.service;

import com.emi.fhqproject.model.UserModel;

public interface UserCategoryService extends PersistenceService<UserModel, String> {
    
}