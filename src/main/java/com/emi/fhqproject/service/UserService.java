package com.emi.fhqproject.service;

import com.emi.fhqproject.model.UserModel;
import com.emi.fhqproject.model.UserRequestModel;

import org.springframework.security.core.userdetails.UserDetailsService;

public interface UserService extends UserDetailsService {
    UserModel register (UserRequestModel requestModel);

}

