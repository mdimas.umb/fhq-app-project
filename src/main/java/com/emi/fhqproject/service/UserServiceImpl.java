package com.emi.fhqproject.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.HttpServerErrorException;

import com.emi.fhqproject.entity.Role;
import com.emi.fhqproject.entity.User;
import com.emi.fhqproject.entity.Persistence.Status;
import com.emi.fhqproject.entity.Role.RoleName;
import com.emi.fhqproject.model.UserModel;
import com.emi.fhqproject.model.UserRequestModel;
import com.emi.fhqproject.repository.RoleRepo;
import com.emi.fhqproject.repository.UserRepo;


@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserRepo userRepo;

    @Autowired
    private RoleRepo roleRepo;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    @Transactional (readOnly = true, propagation = Propagation.SUPPORTS)
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		User user = userRepo.findByUsername(username);
		Collection<GrantedAuthority> grantedAuthorities = new ArrayList<>();
		user.getRoles().forEach(role -> {
			GrantedAuthority grantedAuthority = new SimpleGrantedAuthority(role.getRoleName());
			grantedAuthorities.add(grantedAuthority);
		});
        return new org.springframework.security.core.userdetails.User(user.getEmail(), user.getPassword(), grantedAuthorities);
    }
    @Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public UserModel register(UserRequestModel requestModel) {
		User userByUsername = userRepo.findByUsername(requestModel.getUsername());
		if (userByUsername != null && userByUsername.getId() != null)
			throw new HttpServerErrorException(HttpStatus.BAD_REQUEST, "Username already exists");
		User userByEmail= userRepo.findByUsername(requestModel.getEmail());
		if (userByEmail != null && userByEmail.getId() != null)
			throw new HttpServerErrorException(HttpStatus.BAD_REQUEST, "Email already exists");
		
		Role role = roleRepo.findByRoleName(RoleName.ROLE_USER.toString());
        User newUser = new User();
        newUser.setName(requestModel.getName());
        newUser.setEmail(requestModel.getEmail());
        newUser.setPassword(passwordEncoder.encode(requestModel.getPassword()));
        newUser.setPhone(requestModel.getPhone());
        newUser.setRoles(Collections.singleton(role));
        newUser.setStatus(Status.ACTIVE);
        newUser.setUsername(requestModel.getUsername());

        newUser = userRepo.save(newUser);

        UserModel userModel = new UserModel();
        BeanUtils.copyProperties(newUser, userModel);
		return userModel;
    }
}
	



	