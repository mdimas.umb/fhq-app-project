package com.emi.fhqproject.service;

import com.emi.fhqproject.entity.User;
import com.emi.fhqproject.entity.Persistence.Status;
import com.emi.fhqproject.model.UserModel;
import com.emi.fhqproject.repository.UserRepo;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.HttpServerErrorException;

@Service
public class UserCategoryServiceImpl implements UserCategoryService {

    @Autowired
	private UserRepo userRepo;

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public UserModel saveOrUpdate(UserModel entity) {
		User user;
		if (entity.getId() != null) {
			user = userRepo.findById(entity.getId()).orElse(null);
			if (user == null)
				throw new HttpServerErrorException(HttpStatus.BAD_REQUEST, " User with id: " + entity.getId() + " not found");
			
            user.setUsername(entity.getUsername());
            user.setPassword(entity.getPassword());
            user.setName(entity.getName());
            user.setEmail(entity.getEmail());
            user.setPhone(entity.getPhone());
			user = userRepo.save(user);
		} else {
			user = new User();
			user.setUsername(entity.getUsername());
            user.setPassword(entity.getPassword());
            user.setName(entity.getName());
            user.setEmail(entity.getEmail());
            user.setPhone(entity.getPhone());
			user = userRepo.save(user);
		}
		BeanUtils.copyProperties(user, entity);
		return entity;
    }
    
    @Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public UserModel delete(UserModel entity) {
		if (entity.getId() != null) {
			User user = userRepo.findById(entity.getId()).orElse(null);
			if (user == null)
				throw new HttpServerErrorException(HttpStatus.BAD_REQUEST, "User with id: " + entity.getId() + " not found");

			if (user.getRoles() != null && user.getRoles().size() > 0)
				throw new HttpServerErrorException(HttpStatus.BAD_REQUEST, "User cannot be deleted because already used");
			
			user.setStatus(Status.NOT_ACTIVE);
			user = userRepo.save(user);
			BeanUtils.copyProperties(user, entity);
			return entity;
		} else
			throw new HttpServerErrorException(HttpStatus.BAD_REQUEST, "Id cannot be null");
    }

    @Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public UserModel deleteById(String id) {
		if (id != null) {
			UserModel entity = new UserModel();
			User user = userRepo.findById(id).orElse(null);
			if (user == null)
				throw new HttpServerErrorException(HttpStatus.BAD_REQUEST, "User with id: " + id + " not found");
			
			if (user.getRoles() != null && user.getRoles().size() > 0)
				throw new HttpServerErrorException(HttpStatus.BAD_REQUEST, "User cannot be deleted because already used");
			
			user.setStatus(Status.NOT_ACTIVE);
			user = userRepo.save(user);
			BeanUtils.copyProperties(userRepo, entity);
			return entity;
		} else
			throw new HttpServerErrorException(HttpStatus.BAD_REQUEST, "Id cannot be null");
	}
    
    @Override
	@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
	public UserModel findById(String id) {
		if (id != null) {
			UserModel entity = new UserModel();
			User user = userRepo.findById(id).orElse(null);
			if (user == null)
				throw new HttpServerErrorException(HttpStatus.BAD_REQUEST, "User with id: " + id + " not found");
			
			BeanUtils.copyProperties(user, entity);
			return entity;
		} else
			throw new HttpServerErrorException(HttpStatus.BAD_REQUEST, "Id cannot be null");
	}

	@Override
	@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
	public List<UserModel> findAll() {
		List<UserModel> entities = new ArrayList<>();
		userRepo.findAll().forEach(data -> {
			UserModel entity = new UserModel();
			BeanUtils.copyProperties(data, entity);
			entities.add(entity);
		});
		return entities;
	}

    @Override
	@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
	public Long countAll() {
		return userRepo.count();
	}   
}