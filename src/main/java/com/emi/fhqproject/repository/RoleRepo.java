package com.emi.fhqproject.repository;

import com.emi.fhqproject.entity.Role;

import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepo extends JpaRepository<Role, String> {
    Role findByRoleName (String RoleName);
}