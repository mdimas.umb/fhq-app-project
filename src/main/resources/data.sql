INSERT INTO public.role (role_name) VALUES ('ROLE_ADMIN');
INSERT INTO public.role (role_name) VALUES ('ROLE_USER');

INSERT INTO public."user" (id, created_by, date_created, date_deleted, date_modified, status, email, name, password, phone, username) VALUES (1, 'init', '2020-06-28 00:00:00', '2020-06-28 00:00:00', '2020-06-28 00:00:00', 'ACTIVE', 'admin@email.com', 'Administrator', '$2y$12$YFUzZyhcri4CK0BFfQXGEu485lQ/joalJtFjRShGNPoNvroLwqI9C', '081298097861', 'admin');
INSERT INTO public."user" (id, created_by, date_created, date_deleted, date_modified, status, email, name, password, phone, username) VALUES (2, 'init', '2020-06-28 00:00:00', '2020-06-28 00:00:00', '2020-06-28 00:00:00', 'ACTIVE', 'user@email.com', 'Siswa', '$2y$12$SuVvhFrtbGJ5H/EDANv4Xe3.PPuQYPySoIrFGYaKxR16DUfgkn.rS', '081298097800', 'siswa');

INSERT INTO public.user_role (username, role_name) VALUES ('admin', 'ROLE_ADMIN');
INSERT INTO public.user_role (username, role_name) VALUES ('siswa', 'ROLE_USER');